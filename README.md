# Wardrobify

Team:

* Person 1 - Aaron Duque
* Person 2 - Timothy Sayson

## Design

## Shoes microservice

A shoe will have properties such as:
-manufacturer : manufacturer of the shoe
-model name: model name for the shoe
-primary color: primary color of the shoe
-secondary color: secondary color of the shoe
-picture url: picture of the shoe

A shoe belongs to ONE bin, and a bin can have MANY shoes.

Bins will be identified by the closet it belongs to, and its bin number.

ex :
Salomon XT 6 {
    manufacturer: Salomon,
    model: XT6,
    primary_color: Black
    secondary_color: Grey
    picture_url: http://example.com
}
bin : {
    name : Main bin 1
    import_href : api/bins/1
}


How the bin from the API is polled :

Bin {
    closet name : main,
    bin number : 1
    bin size : 10
}

BinVo : {
    name : closet_name + bin + Bin[bin_number],
    bin_href: Bin[href],
}
## Hats microservice

A hat will have properties such as:

fabric: the material from which the hat is made
style_name: style or model name of the hat
color: color of the hat
A hat belongs to ONE location, and a location can have MANY hats.

Locations will be identified by the closet name it belongs to, and its section number.

For example:

Fedora {
    "fabric": "Felt",
    "style_name": "Fedora",
    "color": "Red"
}
location : {
    "closet_name": "test1",
	"section_number":"1",
	"shelf_number":"1"
}

How the location from the API is polled :

Location {
    "closet_name": "test1",
	"section_number":"1",
	"shelf_number":"1"
}

LocationVO
  
    location:["href"],
    name:location["closet_name"]




