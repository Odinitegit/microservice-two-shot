# Generated by Django 4.0.3 on 2023-12-15 03:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locationvo',
            old_name='style_name',
            new_name='name',
        ),
    ]
