from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

class LocationVOListEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]



class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = {
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id"
        

    }
    encoders = {"location": LocationVODetailEncoder()}



class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
         "fabric",
         "style_name",
         "color",
         "picture_url",
         "location",
         "id"
    ]
    encoders = {"location": LocationVOListEncoder()}



@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse({
            "hats":hats},
            encoder = HatListEncoder,safe = False
            )

    else:
        content = json.loads(request.body)
        print(LocationVO.objects.all())
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(id=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid id"},
                status=400,
           )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
            )


@require_http_methods(["GET", "DELETE"])
def hat_details(request, id):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False


            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Not here"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=id)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: 
            content = json.loads(request.body)
            try:

                hat = Hats.objects.get(id=id)

                props = [
                    "fabric",
                    "style_name",
                    "color",
                    "picture_url",
                    ]
                for prop in props:
                    if prop in content:
                        setattr(hat, prop, content[prop])
                hat.save()
                return JsonResponse(
                    hat,
                    encoder=HatDetailEncoder,
                    safe=False,
                )
            except Hats.DoesNotExist:
                response = JsonResponse({"message": "Does not exist"})
                response.status_code = 404
                return response




@require_http_methods(["GET"])
def locationvo_list(request):
    locations = LocationVO.objects.all()
    return JsonResponse(
            locations,
            encoder=LocationVOListEncoder,
            safe=False
            )
