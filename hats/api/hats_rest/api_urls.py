from django.urls import path

from .api_views import list_hats, hat_details, locationvo_list

urlpatterns = [
    path('list/', list_hats, name="list_hats"),
     path('<int:id>/',hat_details,name ="hat_details"),
    path('new_hat/', list_hats, name="new_hat"),
    path('locations/', locationvo_list, name="locations")

]
