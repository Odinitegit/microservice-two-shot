# Create your models here.
from django.db import models
from django.urls import reverse



class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="locations",
        on_delete=models.CASCADE,
    )


        
    def get_api_url(self):
        return reverse("hat_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return self.style_name
