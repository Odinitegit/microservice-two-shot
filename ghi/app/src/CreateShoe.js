import React, { useState, useEffect } from 'react';

function ShoeForm({ getShoes }) {
  const [manufacturer, setManufacturer] = useState('');
  const [model, setModel] = useState('');
  const [primary_color, setPrimaryColor] = useState('');
  const [secondary_color, setSecondaryColor] = useState('');
  const [bin, setBin] = useState('');
  const [picture_url, setPictureUrl] = useState('');
  const [bins, setBins] = useState([]);
  const [submitted, setHasSubmitted] = useState(false);

  async function fetchBins() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchBins();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      manufacturer,
      model,
      primary_color,
      secondary_color,
      picture_url,
      bin,
    };

    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      setManufacturer('');
      setModel('');
      setPrimaryColor('');
      setSecondaryColor('');
      setBin('');
      setPictureUrl('');
      setHasSubmitted(true);
      getShoes()
    }
  }

  function handleChangeManufacturer(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleChangeModel(event) {
    const { value } = event.target;
    setModel(value);
  }

  function handleChangePrimaryColor(event) {
    const { value } = event.target;
    setPrimaryColor(value);
  }

  function handleChangeSecondaryColor(event) {
    const { value } = event.target;
    setSecondaryColor(value);
  }
  function handleChangePictureUrl(event) {
    const { value } = event.target;
    setPictureUrl(value);
  }

  function handleChangeBin(event) {
    const { value } = event.target;
    setBin(value);
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (submitted) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={model} onChange={handleChangeModel} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="model">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input value={primary_color} onChange={handleChangePrimaryColor} placeholder="primary_color" required type="text" name="primary_color" id="primary_color" className="form-control" />
              <label htmlFor="primary_color">Primary Color</label>
            </div>

            <div className="form-floating mb-3">
              <input value={secondary_color} onChange={handleChangeSecondaryColor} placeholder="Secondary Color" required type="text" name="secondary_color" id="secondary_color" className="form-control" />
              <label htmlFor="secondary_color">Secondary Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={handleChangePictureUrl} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="mb-3">
              <select value={bin} onChange={handleChangeBin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>{bin.closet_name} {bin.bin_number}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          <div className="my-5 px-2 container">
    <div className={messageClasses} id="success-message">
                New Shoe Succesfully Created!
    </div>
        </div>
      </div>
    </div>
    </div>
  );
}

export default ShoeForm;
