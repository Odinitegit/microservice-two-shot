import { BrowserRouter, Routes, Route } from 'react-router-dom';
import {useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './Shoes';
import ShoeForm from './CreateShoe';
import HatList from './HatList'
import NewHatForm from './NewHatForm';


function App(props) {
  const [shoes, setShoes ] = useState([]);
  const [bins, setBins ] = useState([]);
  const [hats, setHats] = useState([]);
  const [locations,setLocations] = useState([]);


  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getBins() {
    const url = ('http://localhost:8100/api/bins/');
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getHats() {
    const response = await fetch('http://localhost:8090/hats/list/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error('An error occurred fetching the data')
    }
  }
  


  async function getLocations() {
    const url = ('http://localhost:8100/api/locations/');
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    } else {
      console.error('An error occurred fetching the data')
    }
  }



  useEffect(() => {
    getShoes();
    getBins();
    getHats();
    getLocations();

  }, [])

  if (shoes === undefined) {
    return null;
  }
  if (hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes} />}/>
            <Route path="new" element={<ShoeForm bins={bins} getBins={getBins} shoes={shoes} getShoes={getShoes} />} />
            </Route>
            <Route path="hats">
              <Route index element={<HatList hats={hats} getHats={getHats}/>}/>
              <Route path="new_hat" element={<NewHatForm locations={locations} getLocations={getLocations}  hats={hats} getHats={getHats} />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>

  );
}
            


export default App;

