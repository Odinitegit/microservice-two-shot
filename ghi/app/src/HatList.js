import { useEffect, useState } from 'react';





function HatsList(props) {
  const [deleted, setDelete] = useState(false)
  const [hats, setHats] = useState([])


  const getData = async () => {
    const response = await fetch('http://localhost:8090/hats/list/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

 
  const deleteHat = async (id, getHats, setDelete ) => {
  
    const url = `http://localhost:8090/hats/${id}/`
    const fetchConfig = {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        getHats();
        setDelete(true)
        window.location.reload();
    }
};





  useEffect(()=>{
    getData()
  }, [deleted])
  

 
 return (

<table className="table table-striped">

    <thead>
          <tr> 
             <th>Name</th> 
             <th>Style</th> 
             <th>Color</th> 
             <th>Fabric</th> 
             <th>Location</th>
             <th>Your Hat</th>
          </tr>
      </thead>

   <tbody>
   {hats.map((hats, index) => {
          let rowName = `Hat ${index + 1}`;
          return (
         
            <tr key={hats.id} >
              <td>{rowName}</td>
              <td>{ hats.style_name }</td>
              <td>{ hats.color }</td>
              <td>{ hats.fabric }</td>
              <td>{ hats.location.name }</td>
              <td><img src={ hats.picture_url } className="img-fluid rounded w-auto"></img></td>
              <td><button type="button" onClick={() => deleteHat(hats.id,props.getHats,setDelete)} className="btn btn-danger">
                    Delete
                  </button>
                  </td>
            </tr>
         
          );
        })}
      </tbody>

      </table>
  );
}

export default HatsList;

