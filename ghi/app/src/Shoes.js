import React, { useState, useEffect } from 'react';

  const deleteShoe = async (href, getShoes, setDelete ) => {
        const url = `http://localhost:8080/${href}`
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getShoes();
            setDelete(true)
        }
    };

    function ShoeList(props) {
        const [deleted, setDelete] = useState(false)
        let messageClasses = 'alert alert-success d-none mb-0';
        useEffect(() => {
        },
         [deleted]);
         if (deleted) {
            messageClasses = 'alert alert-success mb-0';
        }
    return (
        <div className="my-1">
        <h1 className="display-5 fw-bold">Shoes</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr key={ shoe.href }>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model }</td>
                <td>{ shoe.primary_color }</td>
                <td>{ shoe.bin.name } </td>
                <td><img src={ shoe.picture_url } className="img-fluid rounded w-auto"></img></td>
                <td><button type="button" onClick={() => deleteShoe(shoe.href, props.getShoes, setDelete)} className="btn btn-dark">
                    Delete
                  </button>
                  </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="my-5 px-2 container">
        {deleted && (
    <div className={messageClasses} id="success-message">
            Shoe succesfully deleted.
    </div>
        )}
    </div>
    </div>
    );
  }

  export default ShoeList;
