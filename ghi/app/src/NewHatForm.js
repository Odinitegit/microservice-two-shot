import React, {useState,useEffect} from 'react';

function NewHatForm(){
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: '',
    });




    const getData = async () => {
      const url = 'http://localhost:8100/api/locations/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    }

    useEffect(() => {
      getData();
    }, []);



    const [hasCreatedNewHat, sethasCreatedNewHat] = useState(false)


    const handleSubmit = async (event) => {
        event.preventDefault();
        const new_hat_url = `http://localhost:8090/hats/new_hat/`



        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(new_hat_url, fetchConfig);
        if(response.ok){
            setFormData({
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: '',
            });


            sethasCreatedNewHat(true);
        }

    };



    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      };

      const formClasses = (!hasCreatedNewHat) ? '' : 'd-none';
      const messageClasses = (!hasCreatedNewHat) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

      return (
        <div className="my-5">
        <div className="row">


          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form className={formClasses} onSubmit={handleSubmit} id="create-new-hat-form">
                  <h1 className="card-title">New Hat Form</h1>
                  <p className="mb-3">
                    Please enter New Hat Details.
                  </p>

                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="fabric" type="text" id="fabric" name="fabric" className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                      </div>

                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="style name" type="text" id="style_name" name="style_name" className="form-control" />
                        <label htmlFor="style_name">Style Name</label>
                      </div>
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="color" type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                      </div>
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="picture_url" type="text" id="picture_url" name="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture Url</label>
                      </div>
                    </div>
                    <div className="mb-3">
                <select onChange={handleFormChange} required name="location" id="location" className="form-select" value = {formData.location}>
                 <option value="">Choose a Wardrobe location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
                  </div>
                  <button className="btn btn-lg btn-primary">Submit New Hat.</button>
                </form>

                <div className={messageClasses} id="success-message">
                  New Hat Created!
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }



      export default NewHatForm;
