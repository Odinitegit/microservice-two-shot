from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import Shoe, BinVO


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href", "id"]

class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = ["name","import_href" ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer",
                  "model",
                  "picture_url",
                  "primary_color",
                  "bin",
                  ]
    encoders = {
         "bin": BinVODetailEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model",
        "picture_url",
        "primary_color",
        "secondary_color",
        "bin",
                ]
    encoders = {
         "bin": BinVOListEncoder(),
    }


# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
 if request.method == "GET":
        if bin_vo_id is not None:
         shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
           shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder, safe=False)

 else:
     content = json.loads(request.body)

 try:
        bin_href = content["bin"]
        bin = BinVO.objects.get(id=bin_href)
        content["bin"] = bin
 except BinVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid bin id"},
        status=400,
    )

 shoe = Shoe.objects.create(**content)
 return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False,)



@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, pk):
    if request.method == "GET":
     shoe = Shoe.objects.get(id=pk)
     return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET"])
def api_list_bins(request):
    if request.method == "GET":
     bins = BinVO.objects.all()
     return JsonResponse(bins, encoder=BinVOListEncoder, safe=False)
