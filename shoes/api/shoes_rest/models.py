from django.db import models
from django.urls import reverse
# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    primary_color = models.CharField(max_length=200)
    secondary_color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=200)


    bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_shoe_detail", kwargs={"pk": self.pk})
